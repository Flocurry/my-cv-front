import { useEffect, useReducer } from 'react'
import axios from 'axios'

export type TState = {
  datas: {}
  isLoading: boolean
  error?: string
}

type TAction =
  | { type: 'request' }
  | { type: 'success'; results: object }
  | { type: 'failure'; error: string }

function reducer(state: TState, action: TAction): TState {
  switch (action.type) {
    case 'request':
      return { ...state, isLoading: true, datas: {} }
    case 'success':
      return { ...state, isLoading: false, datas: action.results }
    case 'failure':
      return {
        ...state,
        isLoading: false,
        datas: {},
        error: action.error
      }
    default:
      return state
  }
}

export const useFetch = (url: string, query: string): TState => {
  // State
  const [{ datas = {}, isLoading, error }, dispatch] = useReducer(reducer, {
    datas: {},
    isLoading: false
  })

  useEffect(() => {
    const fetchDatas = async () => {
      const response = await axios.post(
        url,
        {
          query,
          variables: {}
        },
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      )
      return response?.data?.data
    }

    dispatch({ type: 'request' })
    fetchDatas()
      .then(results => dispatch({ type: 'success', results }))
      .catch(err => dispatch({ type: 'failure', error: err.message }))
  }, [query, url])

  return { datas, isLoading, error }
}
