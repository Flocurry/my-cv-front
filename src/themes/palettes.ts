import { PaletteMode } from '@mui/material'
import { grey } from '@mui/material/colors'

export const getPalette = (mode: PaletteMode) => ({
  palette: {
    mode,
    ...(mode === 'light'
      ? {
          primary: {
            main: '#42a5f5',
            dark: '#0077c2',
            light: '#80d6ff',
            contrastText: '#fff'
          },
          divider: grey[400],
          background: {
            default: grey[300],
            paper: '#fff'
          },
          text: {
            primary: '#000000'
          }
        }
      : {
          primary: {
            main: '#42a5f5',
            dark: '#0077c2',
            light: '#80d6ff',
            contrastText: '#000000'
          },
          divider: grey[700],
          background: {
            default: '#000000',
            paper: '#1d2226'
          },
          text: {
            primary: '#fff'
          }
        })
  }
})
