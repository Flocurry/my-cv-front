import * as React from 'react'
// Own component
import { Loisirs } from './Loisirs'
// Datas
import { datas } from './datas'

export const LoisirsContainer: React.FC = () => <Loisirs datas={datas} />
