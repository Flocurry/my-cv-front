import React from 'react'
import { useTheme } from '@mui/material'
import tennisIcon from '@iconify/icons-mdi/tennis'
import { v4 as uuidv4 } from 'uuid'
// Own components
import { MyPaper } from '../utils/MyPaper'
import { Info } from '../utils/Info'
import { MyTypography } from '../utils/MyTypography'
import { MyAvatar } from '../utils/MyAvatar'

type LoisirsProps = {
  datas: {
    data: string
    icon: object
  }[]
}

export const Loisirs: React.FC<LoisirsProps> = ({ datas }) => {
  // Theme
  const theme = useTheme()

  return (
    <MyPaper
      rubrique={
        <MyTypography
          customStyle={{ fontWeight: 800, color: theme.palette.primary.main }}
        >
          LOISIRS
        </MyTypography>
      }
      icon={<MyAvatar icon={tennisIcon} />}
      paperStyle={{
        backgroundColor: theme.palette.background.paper
      }}
    >
      {datas.map(elt => (
        <Info
          key={uuidv4()}
          icon={elt.icon}
          data={elt.data}
          color={theme.palette.primary.main}
        />
      ))}
    </MyPaper>
  )
}
