import basketballIcon from '@iconify/icons-mdi/basketball'
import cookieIcon from '@iconify/icons-mdi/cookie'
import movieOpen from '@iconify/icons-mdi/movie-open'
import monitorEye from '@iconify/icons-mdi/monitor-eye'

export const datas = [
  {
    data: 'Basket',
    icon: basketballIcon
  },
  {
    data: 'Cuisine',
    icon: cookieIcon
  },
  {
    data: 'Cinéma',
    icon: movieOpen
  },
  {
    data: 'Veille techno',
    icon: monitorEye
  }
]
