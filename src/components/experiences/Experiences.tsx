import React from 'react'
import { useTheme } from '@mui/material'
import briefcaseIcon from '@iconify/icons-mdi/briefcase'
// Own components
import { MyPaper } from '../utils/MyPaper'
import { MyTypography } from '../utils/MyTypography'
import { StepperExperiences } from './StepperExperiences'
import { MyAvatar } from '../utils/MyAvatar'

export const Experiences: React.FC = () => {
  // Theme
  const theme = useTheme()

  return (
    <MyPaper
      rubrique={
        <MyTypography
          customStyle={{ fontWeight: 800, color: theme.palette.primary.main }}
        >
          EXPERIENCES
        </MyTypography>
      }
      icon={<MyAvatar icon={briefcaseIcon} />}
      paperStyle={{
        backgroundColor: theme.palette.background.paper
      }}
    >
      <StepperExperiences />
    </MyPaper>
  )
}
