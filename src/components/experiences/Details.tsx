import React from 'react'
import { styled } from '@mui/material'
import { v4 as uuidv4 } from 'uuid'
// Own component
import { MyTypography } from '../utils/MyTypography'

const PREFIX = 'Details'

const classes = {
  ul: `${PREFIX}-ul`
}

const Root = styled('ul')({
  [`&.${classes.ul}`]: {
    marginTop: 0,
    marginBottom: 5,
    fontSize: 13
  }
})

type DetailsProps = {
  details: string[]
}

export const Details: React.FC<DetailsProps> = ({ details }) => (
  <Root className={classes.ul}>
    {details.map(detail => (
      <li style={{ whiteSpace: 'pre-wrap' }} key={uuidv4()}>
        <MyTypography variant="body2">{detail}</MyTypography>
      </li>
    ))}
  </Root>
)
