import React from 'react'
import { styled, Box } from '@mui/material'
import { v4 as uuidv4 } from 'uuid'
// Own component
import { MyTypography } from '../utils/MyTypography'

const PREFIX = 'StepperLabel'

const classes = {
  root: `${PREFIX}-root`
}

const Root = styled(Box)(({ theme }) => ({
  [`&.${classes.root}`]: {
    marginLeft: 5
  }
}))

export interface StepperLabelProps {
  poste: string
  lieux: string[]
}

export const StepperLabel: React.FC<StepperLabelProps> = ({ poste, lieux }) => (
  <Root className={classes.root}>
    <MyTypography customStyle={{ fontWeight: 600 }}>{poste}</MyTypography>
    {lieux.map(lieu => (
      <MyTypography key={uuidv4()}>{lieu}</MyTypography>
    ))}
  </Root>
)
