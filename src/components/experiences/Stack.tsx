import React from 'react'
import { styled, Box } from '@mui/material'
// Own component
import { MyTypography } from '../utils/MyTypography'

const PREFIX = 'Stack'

const classes = {
  root: `${PREFIX}-root`
}

const Root = styled(Box)({
  [`&.${classes.root}`]: {
    display: 'flex',
    flexDirection: 'column'
  }
})

export interface StackProps {
  stack: string
}

export const Stack: React.FC<StackProps> = ({ stack }) => (
  <Root className={classes.root}>
    <MyTypography variant="caption">
      <u>Stack technique</u>
    </MyTypography>
    <MyTypography variant="caption">{stack}</MyTypography>
  </Root>
)
