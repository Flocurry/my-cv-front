export const datas = [
  {
    date: ['Actuel', 'Nov. 2018'],
    lieux: ['Campus Thales Bordeaux, 33700 Mérignac'],
    poste: 'Développeur Web Full Stack',
    details: [
      'Référent technique React JS.',
      'Etude comparative sur le choix de la technologie (ReactJS ou Angular).',
      'Conduite au changement et migration d’un framework interne vers React JS.',
      'Management et encadrement de développeurs Junior.',
      'Entretiens techniques de recrutement.',
      "Développement d'un Backend for front en Symfony qui permet de formater\nles données renvoyées par une API en Symfony pour la lib datagrid.",
      "Développement des endpoints de l'API.",
      'Corrections et évolutions apportées sur les applications.',
      'Mise en place de tests de composants avec Jest/Enzyme.',
      'Maintenance correctives et évolutives sur les applications via l’outil Mantis.'
    ],
    stack:
      'Linux(Debian) - Windows10 - ReactJS/Redux/Redux-Thunk/MaterialUI/Symfony - Mysql8 – Git - Mantis'
  },
  {
    date: ['Oct. 2018', 'Août 2017'],
    lieux: ['SQLI'],
    poste: 'Développeur Web Full Stack',
    details: [
      'Maintenance corrective et évolutive sur une application dockerisée (php 5.3 + Apache 2.2).',
      'Création d’un nouveau Docker pour la nouvelle version de l’application (php 5.6 + Apache 2.4).',
      'Rédaction du Dockerfile et docker-compose.',
      'Corrections et évolutions apportées sur l’application (connexion à partir d’un Ldap, mise en place d’un WebSSO).'
    ],
    stack: 'Linux(Debian) - PHP5 - Mysql8 – Docker – Git – Apache'
  },
  {
    date: ['Juil. 2017', 'Juin 2012'],
    lieux: ['Orange, 33320 Eysines', 'Orange, 33600 Pessac'],
    poste: 'Développeur Web Full Stack',
    details: [
      'Analyse des spécifications techniques détaillées transmises par le client.',
      'Participation au cycle de développement.',
      'Corrections et évolutions apportées sur l’application.',
      'Daily SCRUM avec le chef de projet et toute l’équipe de dev.',
      'Présentation des demos aux utilisateurs tous les vendredis.',
      'Tests unitaires avec PHPUnit.'
    ],
    stack: 'Windows - HTML/CSS - JQuery - PHP5 – PHPUNIT - Mysql8 – Git - Agile'
  }
]
