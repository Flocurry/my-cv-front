import React from 'react'
import { styled, Box } from '@mui/material'
import { v4 as uuidv4 } from 'uuid'
// Own component
import { MyTypography } from '../utils/MyTypography'

const PREFIX = 'StepIcon'

const classes = {
  root: `${PREFIX}-root`,
  divdates: `${PREFIX}-divdates`,
  divdate: `${PREFIX}-divdate`,
  circle: `${PREFIX}-circle`
}

const Root = styled(Box)(({ theme }) => ({
  [`&.${classes.root}`]: {
    display: 'flex',
    height: 22,
    alignItems: 'center',
    color: theme.palette.primary.main,
    marginLeft: 8
  },

  [`& .${classes.divdates}`]: {
    display: 'flex',
    flexDirection: 'column',
    marginRight: 10,
    minWidth: 58
  },

  [`& .${classes.divdate}`]: {
    color: theme.palette.text.primary
  },

  [`& .${classes.circle}`]: {
    width: 10,
    height: 10,
    borderRadius: '50%',
    backgroundColor: 'currentColor',
    margin: 6
  }
}))

export interface StepIconProps {
  dates: string[]
}

export const StepIcon: React.FC<StepIconProps> = ({ dates }) => (
  <Root className={classes.root}>
    <Box className={classes.divdates}>
      {dates.map(date => (
        <Box key={uuidv4()} className={classes.divdate}>
          <MyTypography variant="caption">
            <i>{date}</i>
          </MyTypography>
        </Box>
      ))}
    </Box>
    <Box className={classes.circle} />
  </Root>
)
