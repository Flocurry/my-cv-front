import React from 'react'
import {
  styled,
  Box,
  Stepper,
  Step,
  StepLabel,
  StepContent,
  StepConnector
} from '@mui/material'
import { v4 as uuidv4 } from 'uuid'
import classNames from 'classnames'
// Own components
import { StepIcon } from './StepIcon'
import { datas } from './datas'
import { StepperLabel } from './StepperLabel'
import { Stack } from './Stack'
import { Details } from './Details'

const PREFIX = 'StepperExperiences'

const classes = {
  root: `${PREFIX}-root`,
  stepper: `${PREFIX}-stepper`,
  connectorColor: `${PREFIX}-connectorColor`,
  connectorLine: `${PREFIX}-connectorLine`,
  connectorStep: `${PREFIX}-connectorStep`
}

const Root = styled(Box)(({ theme }) => ({
  [`& .${classes.root}`]: {
    maxWidth: 580
  },

  [`& .${classes.stepper}`]: {
    color: theme.palette.text.primary
  },

  [`& .${classes.connectorColor}`]: {
    borderColor: theme.palette.primary.main
  },

  [`& .${classes.connectorLine}`]: {
    marginLeft: 74
  },

  [`& .${classes.connectorStep}`]: {
    marginLeft: 86
  }
}))

export const StepperExperiences: React.FC = () => (
  <Root className={classes.root}>
    <Stepper
      className={classes.stepper}
      orientation="vertical"
      connector={
        <StepConnector
          classes={{
            line: classNames(classes.connectorLine, classes.connectorColor)
          }}
        />
      }
    >
      {datas.map(experience => (
        <Step active={true} key={uuidv4()}>
          <StepLabel
            StepIconComponent={() => <StepIcon dates={experience.date} />}
          >
            <StepperLabel poste={experience.poste} lieux={experience.lieux} />
          </StepLabel>
          <StepContent
            classes={{
              root: classNames(classes.connectorStep, classes.connectorColor)
            }}
          >
            <Details details={experience.details} />
            <Stack stack={experience.stack} />
          </StepContent>
        </Step>
      ))}
    </Stepper>
  </Root>
)
