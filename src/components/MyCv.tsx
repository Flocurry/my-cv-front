import React from 'react'
import { styled, Box } from '@mui/material'
// Own components
import { Title } from './title/Title'
import { ContactsContainer } from './contacts/ContactsContainer'
import { LoisirsContainer } from './loisirs/LoisirsContainer'
import { Experiences } from './experiences/Experiences'
import { FormationsContainer } from './formations/FormationsContainer'
import { Skills } from './skills/Skills'

const PREFIX = 'MyCv'

const classes = {
  root: `${PREFIX}-root`,
  cvbody: `${PREFIX}-cvbody`,
  contact: `${PREFIX}-contact`
}

const Root = styled(Box)(({ theme }) => ({
  [`&.${classes.root}`]: {
    backgroundColor: theme.palette.background.default,
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  [`& .${classes.cvbody}`]: {
    display: 'flex'
  },
  [`& .${classes.contact}`]: {
    display: 'flex',
    flexDirection: 'column',
    maxWidth: 305
  }
}))

export const MyCv: React.FC = () => (
  <Root className={classes.root}>
    <Box className={classes.cvbody}>
      <Box className={classes.contact}>
        <Title />
        <ContactsContainer />
        <LoisirsContainer />
      </Box>
      <Experiences />
      <Box>
        <FormationsContainer />
        <Skills />
      </Box>
    </Box>
  </Root>
)
