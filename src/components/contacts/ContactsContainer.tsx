import * as React from 'react'
// Own component
import { Contacts } from './Contacts'
// Datas
import { datas } from './datas'

export const ContactsContainer: React.FC = () => <Contacts datas={datas} />
