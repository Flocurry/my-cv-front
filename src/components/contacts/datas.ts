import phoneIcon from '@iconify/icons-mdi/phone'
import emailIcon from '@iconify/icons-mdi/email'
import mapMarkerRadius from '@iconify/icons-mdi/map-marker-radius'

export const datas = [
  {
    data: '06.43.48.20.57',
    icon: phoneIcon
  },
  {
    data: 'floriandarrigand@gmail.com',
    icon: emailIcon
  },
  {
    data: '182 rue Barreyre 33300 Bordeaux',
    icon: mapMarkerRadius
  }
]
