import React from 'react'
import { useTheme } from '@mui/material'
import { v4 as uuidv4 } from 'uuid'
import shieldAccount from '@iconify/icons-mdi/shield-account'
// Own components
import { MyPaper } from '../utils/MyPaper'
import { MyTypography } from '../utils/MyTypography'
import { Info } from '../utils/Info'
import { MyAvatar } from '../utils/MyAvatar'

type ContactsProps = {
  datas: {
    data: string
    icon: object
  }[]
}

export const Contacts: React.FC<ContactsProps> = ({ datas }) => {
  // Theme
  const theme = useTheme()

  return (
    <MyPaper
      rubrique={
        <MyTypography
          customStyle={{ fontWeight: 800, color: theme.palette.primary.main }}
        >
          CONTACTS
        </MyTypography>
      }
      icon={<MyAvatar icon={shieldAccount} />}
      paperStyle={{
        backgroundColor: theme.palette.background.paper
      }}
    >
      {datas.map(elt => (
        <Info
          key={uuidv4()}
          icon={elt.icon}
          data={elt.data}
          color={theme.palette.primary.main}
        />
      ))}
    </MyPaper>
  )
}
