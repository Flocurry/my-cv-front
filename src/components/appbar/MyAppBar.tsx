import React, { ReactNode } from 'react'
import { AppBar, Toolbar, Box } from '@mui/material'

type MyAppBarProps = {
  children: ReactNode
}

export const MyAppBar: React.FC<MyAppBarProps> = ({ children }) => (
  <Box sx={{ flexGrow: 1 }}>
    <AppBar>
      <Toolbar variant="dense">{children}</Toolbar>
    </AppBar>
  </Box>
)
