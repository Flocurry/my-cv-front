import React from 'react'
import { styled, useTheme, Box } from '@mui/material'
import schoolIcon from '@iconify/icons-mdi/school'
import { v4 as uuidv4 } from 'uuid'
// Own components
import { MyPaper } from '../utils/MyPaper'
import { MyTypography } from '../utils/MyTypography'
import { MyAvatar } from '../utils/MyAvatar'

const PREFIX = 'Formations'

const classes = {
  avatar: `${PREFIX}-avatar`,
  formation: `${PREFIX}-formation`,
  date: `${PREFIX}-date`,
  diplomelieu: `${PREFIX}-diplomelieu`
}

const Root = styled(Box)({
  [`&.${classes.formation}`]: {
    display: 'flex'
  },
  [`& .${classes.date}`]: {
    marginRight: 15
  },
  [`& .${classes.diplomelieu}`]: {
    display: 'flex',
    flexDirection: 'column'
  }
})

type FormationsProps = {
  datas: {
    date: string
    diplome: string
    lieu: string
  }[]
}

export const Formations: React.FC<FormationsProps> = ({ datas }) => {
  // Theme
  const theme = useTheme()

  return (
    <MyPaper
      rubrique={
        <MyTypography
          customStyle={{ fontWeight: 800, color: theme.palette.primary.main }}
        >
          FORMATIONS
        </MyTypography>
      }
      icon={<MyAvatar icon={schoolIcon} />}
      paperStyle={{
        backgroundColor: theme.palette.background.paper
      }}
    >
      {datas.map(data => (
        <Root key={uuidv4()} className={classes.formation}>
          <Box className={classes.date}>
            <MyTypography variant="caption">
              <u>{data.date}</u>
            </MyTypography>
          </Box>
          <Box className={classes.diplomelieu}>
            <MyTypography variant="body2">{data.diplome}</MyTypography>
            <MyTypography variant="body2">{data.lieu}</MyTypography>
          </Box>
        </Root>
      ))}
    </MyPaper>
  )
}
