import * as React from 'react'
// Own component
import { Formations } from './Formations'
// datas
import { datas } from './datas'

export const FormationsContainer: React.FC = () => <Formations datas={datas} />
