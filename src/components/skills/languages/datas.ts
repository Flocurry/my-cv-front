export const datas = [
  {
    data: 'Javascript',
    stars: [true, true, true, true, false]
  },
  {
    data: 'HTML5',
    stars: [true, true, true, true, false]
  },
  {
    data: 'CSS3',
    stars: [true, true, true, true, false]
  },
  {
    data: 'PHP',
    stars: [true, true, true, true, false]
  },
  {
    data: 'Typescript',
    stars: [true, true, false, false, false]
  }
]
