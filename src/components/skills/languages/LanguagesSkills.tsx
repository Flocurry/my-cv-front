import React from 'react'
// Own component
import { SubSkills } from '../SubSkills'
// datas
import { datas } from './datas'

export const LanguagesSkills: React.FC = () => (
  <SubSkills label="Languages" datas={datas} />
)
