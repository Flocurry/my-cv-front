import React from 'react'
// Own component
import { SubSkills } from '../SubSkills'
// datas
import { datas } from './datas'

export const BddSkills: React.FC = () => (
  <SubSkills label="Base de données" datas={datas} />
)
