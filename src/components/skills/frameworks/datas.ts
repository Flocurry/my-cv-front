export const datas = [
  {
    data: 'ReactJs',
    stars: [true, true, true, true, false]
  },
  {
    data: 'Redux',
    stars: [true, true, true, true, false]
  },
  {
    data: 'MUI v4/v5',
    stars: [true, true, true, true, false]
  },
  {
    data: 'Symfony 3',
    stars: [true, true, true, true, false]
  }
]
