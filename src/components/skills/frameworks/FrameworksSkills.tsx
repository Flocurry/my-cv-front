import React from 'react'
// Own component
import { SubSkills } from '../SubSkills'
// datas
import { datas } from './datas'

export const FrameworksSkills: React.FC = () => (
  <SubSkills label="Frameworks | Librairies" datas={datas} />
)
