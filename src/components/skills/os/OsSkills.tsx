import React from 'react'
// Own component
import { SubSkills } from '../SubSkills'
// datas
import { datas } from './datas'

export const OsSkills: React.FC = () => <SubSkills label="OS" datas={datas} />
