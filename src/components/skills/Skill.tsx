import React from 'react'
import { styled, useTheme, Box } from '@mui/material'
import { Icon } from '@iconify/react'
import circleIcon from '@iconify/icons-mdi/circle'
import circleOutline from '@iconify/icons-mdi/circle-outline'
import { v4 as uuidv4 } from 'uuid'
// Own component
import { MyTypography } from '../utils/MyTypography'

const PREFIX = 'Skill'

const classes = {
  root: `${PREFIX}-root`,
  divskill: `${PREFIX}-divskill`
}

const Root = styled(Box)({
  [`&.${classes.root}`]: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  [`& .${classes.divskill}`]: {
    minWidth: 140
  }
})

export interface SkillProps {
  data: string
  stars: boolean[]
}

export const Skill: React.FC<SkillProps> = ({ data, stars }) => {
  // Theme
  const theme = useTheme()

  return (
    <Root className={classes.root}>
      <Box className={classes.divskill}>
        <MyTypography variant="body2">{data}</MyTypography>
      </Box>
      <Box>
        {stars.map(star =>
          star ? (
            <Icon
              key={uuidv4()}
              icon={circleIcon}
              width={12}
              height={12}
              color={theme.palette.primary.main}
            />
          ) : (
            <Icon
              key={uuidv4()}
              icon={circleOutline}
              width={12}
              height={12}
              color={theme.palette.primary.main}
            />
          )
        )}
      </Box>
    </Root>
  )
}
