export const datas = [
  {
    data: 'Git',
    stars: [true, true, true, true, false]
  },
  {
    data: 'Docker',
    stars: [true, true, true, false, false]
  },
  {
    data: 'Postman',
    stars: [true, true, true, true, true]
  }
]
