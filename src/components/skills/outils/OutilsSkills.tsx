import React from 'react'
// Own component
import { SubSkills } from '../SubSkills'
// datas
import { datas } from './datas'

export const OutilsSkills: React.FC = () => (
  <SubSkills label="Outils" datas={datas} />
)
