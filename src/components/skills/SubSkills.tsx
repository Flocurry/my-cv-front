import React from 'react'
import { styled, Box, Chip, useTheme } from '@mui/material'
import { v4 as uuidv4 } from 'uuid'
// Own components
import { MyPaper } from '../utils/MyPaper'
import { Skill } from './Skill'

const PREFIX = 'FrameworksSkills'

const classes = {
  root: `${PREFIX}-root`,
  chip: `${PREFIX}-chip`,
  divskill: `${PREFIX}-divskill`
}

const Root = styled(Box)(({ theme }) => ({
  [`&.${classes.root}`]: {
    marginTop: 20
  },
  [`& .${classes.chip}`]: {
    backgroundColor: theme.palette.primary.main,
    marginTop: -22,
    color: theme.palette.background.paper
  },
  [`& .${classes.divskill}`]: {
    marginTop: 15
  }
}))

type SubSkillsProps = {
  label: string
  datas: {
    data: string
    stars: boolean[]
  }[]
}

export const SubSkills: React.FC<SubSkillsProps> = ({ label, datas }) => {
  // Theme
  const theme = useTheme()

  return (
    <Root className={classes.root}>
      <MyPaper
        rubrique={
          <Chip
            label={label}
            size="small"
            classes={{
              root: classes.chip
            }}
          />
        }
        withDivider={false}
        paperStyle={{
          backgroundColor: theme.palette.background.paper,
          border: `1px solid ${theme.palette.divider}`
        }}
      >
        <Box className={classes.divskill}>
          {datas.map(elt => (
            <Skill key={uuidv4()} data={elt.data} stars={elt.stars} />
          ))}
        </Box>
      </MyPaper>
    </Root>
  )
}
