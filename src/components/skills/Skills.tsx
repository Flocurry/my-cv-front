import React from 'react'
import { useTheme } from '@mui/material'
import headCog from '@iconify/icons-mdi/head-cog'
// Own components
import { MyPaper } from '../utils/MyPaper'
import { LanguagesSkills } from './languages/LanguagesSkills'
import { BddSkills } from './bdd/BddSkills'
import { OsSkills } from './os/OsSkills'
import { MyTypography } from '../utils/MyTypography'
import { FrameworksSkills } from './frameworks/FrameworksSkills'
import { OutilsSkills } from './outils/OutilsSkills'
import { MyAvatar } from '../utils/MyAvatar'

export const Skills: React.FC = () => {
  // Theme
  const theme = useTheme()

  return (
    <MyPaper
      rubrique={
        <MyTypography
          customStyle={{ fontWeight: 800, color: theme.palette.primary.main }}
        >
          COMPÉTENCES
        </MyTypography>
      }
      icon={<MyAvatar icon={headCog} />}
      paperStyle={{
        backgroundColor: theme.palette.background.paper
      }}
    >
      <LanguagesSkills />
      <FrameworksSkills />
      <OutilsSkills />
      <BddSkills />
      <OsSkills />
    </MyPaper>
  )
}
