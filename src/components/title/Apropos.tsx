import React from 'react'
import { styled, Box, Chip, useTheme } from '@mui/material'
// Own components
import { MyPaper } from '../utils/MyPaper'
import { ContentApropos } from './ContentApropos'

const PREFIX = 'Apropos'

const classes = {
  root: `${PREFIX}-root`,
  chip: `${PREFIX}-chip`,
  divskill: `${PREFIX}-divskill`
}

const Root = styled(Box)(({ theme }) => ({
  [`&.${classes.root}`]: {
    marginTop: 15
  },

  [`& .${classes.chip}`]: {
    backgroundColor: theme.palette.background.paper,
    marginTop: -22,
    color: theme.palette.primary.main
  },

  [`& .${classes.divskill}`]: {
    marginTop: 15,
    color: theme.palette.primary.contrastText,
    fontSize: 14
  }
}))

export const Apropos: React.FC = () => {
  // Theme
  const theme = useTheme()

  return (
    <Root className={classes.root}>
      <MyPaper
        rubrique={
          <Chip
            label="A propos"
            size="small"
            classes={{
              root: classes.chip
            }}
          />
        }
        withDivider={false}
        paperStyle={{
          backgroundColor: theme.palette.primary.main,
          boxShadow: 'none',
          border: `1px solid ${theme.palette.background.paper}`
        }}
      >
        <Box className={classes.divskill}>
          <ContentApropos />
        </Box>
      </MyPaper>
    </Root>
  )
}
