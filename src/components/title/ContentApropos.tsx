import React from 'react'
import { useTheme } from '@mui/material'
// Own component
import { MyTypography } from '../utils/MyTypography'

export const ContentApropos: React.FC = () => {
  // Theme
  const theme = useTheme()

  return (
    <MyTypography
      variant="body2"
      customStyle={{ color: theme.palette.primary.contrastText }}
    >
      Développeur Full stack <b>ReactJS/Symfony/Mysql</b> possédant une
      expérience de 10 ans, je suis à l'écoute de nouvelles opportunités qui me
      permettraient de parfaire mes compétences autour de l'écosystème React.
    </MyTypography>
  )
}
