import React from 'react'
import { styled, useTheme, Box } from '@mui/material'
// Own component
import { MyTypography } from '../utils/MyTypography'

const PREFIX = 'PrenomNom'

const classes = {
  root: `${PREFIX}-root`,
  typoprenom: `${PREFIX}-typoprenom`
}

const Root = styled(Box)({
  [`&.${classes.root}`]: {
    display: 'flex',
    alignItems: 'baseline'
  },
  [`& .${classes.typoprenom}`]: {
    marginRight: 7
  }
})

type PrenomNomProps = {
  prenom: string
  nom: string
}

export const PrenomNom: React.FC<PrenomNomProps> = ({ prenom, nom }) => {
  // Theme
  const theme = useTheme()

  return (
    <Root className={classes.root}>
      <Box className={classes.typoprenom}>
        <MyTypography
          variant="h6"
          customStyle={{ color: theme.palette.primary.contrastText }}
        >
          {prenom}
        </MyTypography>
      </Box>
      <MyTypography
        variant="h6"
        customStyle={{ color: theme.palette.primary.contrastText }}
      >
        {nom}
      </MyTypography>
    </Root>
  )
}
