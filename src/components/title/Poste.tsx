import React from 'react'
import { useTheme } from '@mui/material'
// Own component
import { MyTypography } from '../utils/MyTypography'

type PosteProps = {
  poste: string
}

export const Poste: React.FC<PosteProps> = ({ poste }) => {
  // Theme
  const theme = useTheme()

  return (
    <MyTypography
      customStyle={{
        fontWeight: 800,
        color: theme.palette.primary.contrastText
      }}
    >
      {poste}
    </MyTypography>
  )
}
