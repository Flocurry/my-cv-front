import React from 'react'
import { styled, Box, Paper } from '@mui/material'
// Own components
import { PrenomNom } from './PrenomNom'
import { Poste } from './Poste'
import { Apropos } from './Apropos'

const PREFIX = 'Title'

const classes = {
  divroot: `${PREFIX}-divroot`,
  paper: `${PREFIX}-paper`
}

const Root = styled(Box)(({ theme }) => ({
  [`&.${classes.divroot}`]: {
    display: 'flex',
    justifyContent: 'center'
  },
  [`& .${classes.paper}`]: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    margin: 10,
    padding: 15,
    backgroundColor: theme.palette.primary.main,
    borderRadius: 15
  }
}))

export const Title: React.FC = () => (
  <Root className={classes.divroot}>
    <Paper className={classes.paper} elevation={0}>
      <PrenomNom prenom="Florian" nom="DARRIGAND" />
      <Poste poste="Ingénieur développeur Web" />
      <Apropos />
    </Paper>
  </Root>
)
