import React from 'react'
import { Box } from '@mui/material'
// Own component
import { MyIcon } from '../../utils/MyIcon'

type SwitchIconProps = {
  icon: object
  color: string
}

export const SwitchIcon: React.FC<SwitchIconProps> = ({ icon, color }) => (
  <Box
    sx={{
      height: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    }}
  >
    <MyIcon icon={icon} width={20} color={color} />
  </Box>
)
