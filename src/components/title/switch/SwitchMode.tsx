import React from 'react'
import { useTheme } from '@mui/material'
import Switch from 'react-switch'
// Icon
import weatherNight from '@iconify/icons-mdi/weather-night'
import whiteBalanceSunny from '@iconify/icons-mdi/white-balance-sunny'
// Own component
import { SwitchIcon } from './SwitchIcon'
import { useModeContext } from '../../../contextes/ModeContext'

export const SwitchMode: React.FC = () => {
  // Theme
  const theme = useTheme()
  // Consomme les données du contexte
  const { mode, setMode } = useModeContext()

  const handleChange = () => {
    setMode(mode === 'dark' ? 'light' : 'dark')
  }

  return (
    <Switch
      onColor={theme.palette.background.paper}
      offColor={theme.palette.background.paper}
      onHandleColor={theme.palette.primary.main}
      offHandleColor={theme.palette.primary.main}
      checkedIcon={
        <SwitchIcon icon={weatherNight} color={theme.palette.primary.light} />
      }
      uncheckedIcon={
        <SwitchIcon
          icon={whiteBalanceSunny}
          color={theme.palette.primary.main}
        />
      }
      onChange={() => handleChange()}
      checked={mode === 'dark'}
    />
  )
}
