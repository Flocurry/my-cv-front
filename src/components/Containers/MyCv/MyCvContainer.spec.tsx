import { act, render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import axios, { AxiosResponse } from 'axios'
// Own component
import { MyCvContainer } from './MyCvContainer'

// Axios mock
jest.mock('axios')
const mockedAxios = axios as jest.Mocked<typeof axios>

// MyCvBody mock for all tests
jest.mock('../../Presentational/MyCvBody/MyCvBody')

afterEach(() => {
  jest.clearAllMocks()
})

describe('MyCvContainer render', () => {
  test('render MyCvBody mock', async () => {
    const mockedResponse: AxiosResponse = {
      data: {},
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    }
    mockedAxios.post.mockResolvedValueOnce(mockedResponse)
    await act(async () => {
      render(<MyCvContainer />)
    })
    expect(screen.getByText('MyCvBody mock')).toBeInTheDocument()
  })
  test('render error message', async () => {
    mockedAxios.post.mockRejectedValueOnce(new Error('Async error'))
    await act(async () => {
      render(<MyCvContainer />)
    })
    expect(
      screen.getByText(
        'Une erreur est survenue lors de la récupération des données'
      )
    ).toBeInTheDocument()
  })
})
