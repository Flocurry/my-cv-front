export const QUERY_MY_CV = `{
    users(firstname: "Florian") {
      firstname,
      lastname,
      poste,
      apropos,
      loisirs {
        label
        icon
      }
    }
  }`
