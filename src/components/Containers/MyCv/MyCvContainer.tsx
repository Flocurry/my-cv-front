import React from 'react'
// Custom hook
import { useFetch } from '../../../hooks/useFetch'
// Own components
import { ErrorFetchMessage } from '../../Presentational/ErrorFetchMessage/ErrorFetchMessage'
import { MyCvBody } from '../../Presentational/MyCvBody/MyCvBody'
// Query fetch datas
import { QUERY_MY_CV } from './query'

export const MyCvContainer: React.FC = () => {
  // Fetch datas
  const URL_API: string = process.env.REACT_APP_URL_API!
  const { datas, isLoading, error } = useFetch(URL_API, QUERY_MY_CV)

  console.log(datas)

  return error !== undefined && !isLoading ? (
    <ErrorFetchMessage />
  ) : (
    <MyCvBody />
  )
}
