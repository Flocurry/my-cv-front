import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
// Own component
import { MyPortfolio } from './MyPortfolio'

// MyAppBar mock
jest.mock('../../appbar/MyAppBar', () => ({
  MyAppBar: () => 'MyAppBar mock'
}))
// MyCvContainer mock
jest.mock('../../Containers/MyCv/MyCvContainer', () => ({
  MyCvContainer: () => 'MyCvContainer mock'
}))

describe('MyPortfolio rendering', () => {
  test('render MyAppBar and MyCv mocks', () => {
    render(<MyPortfolio />)
    expect(screen.getByText('MyAppBar mock')).toBeInTheDocument()
    expect(screen.getByText('MyCvContainer mock')).toBeInTheDocument()
  })
})
