import React from 'react'
import { Box } from '@mui/material'
// Own components
import { MyAppBar } from '../../appbar/MyAppBar'
import { SwitchMode } from '../../title/switch/SwitchMode'
import { MyCvContainer } from '../../Containers/MyCv/MyCvContainer'

export const MyPortfolio: React.FC = () => (
  <>
    <MyAppBar>
      <SwitchMode />
    </MyAppBar>
    <Box sx={{ paddingTop: '48px' }}>
      <MyCvContainer />
    </Box>
  </>
)
