import { forwardRef } from 'react'
import Pdf from 'react-to-pdf'
// Own component
import { ExportToPdfButton } from './ExportToPdfButton/ExportToPdfButton'

type ToPdfType = {
  targetRef: object
  toPdf: () => void
}

const options = {
  orientation: 'landscape',
  unit: 'in',
  format: [18.1, 9.7]
}

export const ExportToPdf = forwardRef<HTMLDivElement>((_props, ref) => (
  <Pdf targetRef={ref} filename="Mon_CV.pdf" options={options} x={-0.9}>
    {(param: ToPdfType) => <ExportToPdfButton onClick={param.toPdf} />}
  </Pdf>
))
