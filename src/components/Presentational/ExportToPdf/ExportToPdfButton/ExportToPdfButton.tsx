import React from 'react'
import { useTheme } from '@mui/material'
// Icon
import filePdfBox from '@iconify/icons-mdi/file-pdf-box'
// Own component
import { MyIcon } from '../../../utils/MyIcon'
// Button style
import { StyledButton } from './StyledButton'

type TExportToPdfButtonProps = {
  onClick: () => void
}

export const ExportToPdfButton: React.FC<TExportToPdfButtonProps> = ({
  onClick
}) => {
  // Theme
  const theme = useTheme()

  return (
    <StyledButton size="large" onClick={onClick}>
      <MyIcon icon={filePdfBox} color={theme.palette.primary.contrastText} />
    </StyledButton>
  )
}
