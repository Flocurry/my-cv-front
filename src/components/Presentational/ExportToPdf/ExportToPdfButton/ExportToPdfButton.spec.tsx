import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import userEvent from '@testing-library/user-event'
// Own component
import { ExportToPdfButton } from './ExportToPdfButton'

// MyIcon mock for all tests
jest.mock('../../../utils/MyIcon', () => ({
  MyIcon: () => 'MyIcon mock'
}))

const onClick = jest.fn()

beforeEach(() => {
  render(<ExportToPdfButton onClick={onClick} />)
})

afterAll(() => {
  jest.clearAllMocks()
})

describe('ExportToPdfButton rendering', () => {
  test('render a button', async () => {
    expect(screen.getByRole('button')).toBeInTheDocument()
  })
  test('render MyIcon mock', async () => {
    expect(screen.getByText('MyIcon mock')).toBeInTheDocument()
  })
})
describe('ExportToPdfButton click', () => {
  test('calls onClick', () => {
    userEvent.click(screen.getByText('MyIcon mock'))
    expect(onClick).toHaveBeenCalledTimes(1)
  })
})
