import { IconButton, styled } from '@mui/material'

export const StyledButton = styled(IconButton)(({ theme }) => ({
  zIndex: 2000,
  position: 'fixed',
  bottom: '15px',
  right: '15px',
  backgroundColor: theme.palette.primary.main,
  '&:hover': {
    backgroundColor: theme.palette.primary.dark,
    animation: 'none'
  },
  '@keyframes pulse': {
    '0%': {
      boxShadow: `0 0 0 0 ${theme.palette.primary.main}30`
    },
    '70%': {
      boxShadow: `0 0 0 15px ${theme.palette.primary.dark}66`
    },
    '100%': {
      boxShadow: `0 0 0 0 ${theme.palette.primary.dark}`
    }
  },
  animation: 'pulse 1s infinite'
}))
