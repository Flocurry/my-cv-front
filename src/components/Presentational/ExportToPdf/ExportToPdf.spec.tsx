import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
// Own component
import { ExportToPdf } from './ExportToPdf'

// ExportToPdfButton mock for all tests
jest.mock('./ExportToPdfButton/ExportToPdfButton', () => ({
  ExportToPdfButton: () => 'ExportToPdfButton mock'
}))

afterEach(() => {
  jest.clearAllMocks()
})

describe('ExportToPdfButton rendering', () => {
  test('render ExportToPdfButton mock', async () => {
    render(<ExportToPdf />)
    expect(screen.getByText('ExportToPdfButton mock')).toBeInTheDocument()
  })
})
