import React from 'react'
// Own component
import { MyTypography } from '../../utils/MyTypography'

type TErrorFetchMessageProps = {
  message?: string
}

export const ErrorFetchMessage: React.FC<TErrorFetchMessageProps> = ({
  message = 'Une erreur est survenue lors de la récupération des données'
}) => <MyTypography>{message}</MyTypography>
