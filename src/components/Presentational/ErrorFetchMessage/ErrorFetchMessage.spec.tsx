import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
// Own component
import { ErrorFetchMessage } from './ErrorFetchMessage'

describe('ErrorFetchMessage rendering', () => {
  test('Check default text', async () => {
    render(<ErrorFetchMessage />)
    expect(
      screen.getByText(
        'Une erreur est survenue lors de la récupération des données'
      )
    ).toBeInTheDocument()
  })
  test('Check props text', async () => {
    render(<ErrorFetchMessage message="TEST" />)
    expect(screen.getByText('TEST')).toBeInTheDocument()
  })
})
