import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
// Own component
import { MyCvBody } from './MyCvBody'

// MyCv mock
jest.mock('../../MyCv', () => ({
  MyCv: () => 'MyCv mock'
}))
// ExportToPdf mock
jest.mock('../ExportToPdf/ExportToPdf', () => ({
  ExportToPdf: () => 'ExportToPdf mock'
}))
// useRef mock
jest.mock('react', () => ({
  ...jest.requireActual('react'),
  useRef: jest.fn()
}))

describe('MyCvBody rendering', () => {
  test('render MyCv and ExportToPdf mocks', () => {
    render(<MyCvBody />)
    expect(screen.getByText('MyCv mock')).toBeInTheDocument()
    expect(screen.getByText('ExportToPdf mock')).toBeInTheDocument()
  })
})
