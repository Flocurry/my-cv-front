import React, { useRef } from 'react'
import { Box } from '@mui/material'
// Own components
import { ExportToPdf } from '../ExportToPdf/ExportToPdf'
import { MyCv } from '../../MyCv'

export const MyCvBody: React.FC = () => {
  // ref
  const ref = useRef<HTMLDivElement>(null)
  return (
    <>
      <ExportToPdf ref={ref} />
      <Box ref={ref}>
        <MyCv />
      </Box>
    </>
  )
}
