import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
// Icon
import filePdfBox from '@iconify/icons-mdi/file-pdf-box'
// Own component
import { MyIcon } from './MyIcon'

describe('MyIcon rendering', () => {
  test('Check svg default height/width 30px', async () => {
    render(<MyIcon icon={filePdfBox} />)
    expect(screen.getByTestId('my-icon').getAttribute('height')).toBe('30')
    expect(screen.getByTestId('my-icon').getAttribute('width')).toBe('30')
  })

  test('Check svg height/width props', async () => {
    render(<MyIcon icon={filePdfBox} width={50} />)
    expect(screen.getByTestId('my-icon').getAttribute('height')).toBe('50')
    expect(screen.getByTestId('my-icon').getAttribute('width')).toBe('50')
  })

  test('Check svg default color', async () => {
    const { container } = render(<MyIcon icon={filePdfBox} />)
    expect(container.firstChild?.firstChild).toHaveAttribute('fill', 'white')
  })

  test('Check svg color props', async () => {
    const { container } = render(<MyIcon icon={filePdfBox} color="red" />)
    expect(container.firstChild?.firstChild).toHaveAttribute('fill', 'red')
  })
})
