import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
// Own component
import { MyTypography } from './MyTypography'

describe('MyTypography rendering', () => {
  test('Check children text', async () => {
    render(<MyTypography>TEST</MyTypography>)
    expect(screen.getByTestId('my-typography')).toHaveTextContent('TEST')
  })
})
