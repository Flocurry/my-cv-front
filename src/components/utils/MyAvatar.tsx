import React from 'react'
import { styled, useTheme, Avatar } from '@mui/material'
// Own component
import { MyIcon } from './MyIcon'

const PREFIX = 'MyAvatar'

const classes = {
  avatar: `${PREFIX}-avatar`
}

const StyledAvatar = styled(Avatar)(({ theme }) => ({
  [`&.${classes.avatar}`]: {
    backgroundColor: theme.palette.primary.main
  }
}))

type MyAvatarProps = {
  icon: object
  color?: string
}

export const MyAvatar: React.FunctionComponent<MyAvatarProps> = ({
  icon,
  color
}) => {
  // Theme
  const theme = useTheme()

  return (
    <StyledAvatar
      classes={{
        root: classes.avatar
      }}
    >
      <MyIcon icon={icon} color={color ?? theme.palette.background.paper} />
    </StyledAvatar>
  )
}
