import { ReactNode } from 'react'
import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
// Owns components
import { Info } from './Info'
import { MyIcon } from './MyIcon'

// MyIcon mock
jest.mock('./MyIcon')
const mockedIcon = MyIcon as jest.Mock<ReactNode>

describe('MyIcon rendering', () => {
  test('render MyIcon mock and text TEST', async () => {
    // On mock l'implémentation unitairement pour ce test
    mockedIcon.mockImplementation(() => 'MyIcon mock')
    render(<Info icon={{}} data="TEST" color="red" />)
    expect(screen.getByText('MyIcon mock')).toBeInTheDocument()
    expect(screen.getByTestId('my-typography')).toHaveTextContent('TEST')
  })
})
