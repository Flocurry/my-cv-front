import React from 'react'
import { Icon } from '@iconify/react'

export type MyIconProps = {
  icon: object
  width?: number
  color?: string
}

export const MyIcon: React.FC<MyIconProps> = ({
  icon,
  width = 30,
  color = 'white'
}) => <Icon data-testid="my-icon" icon={icon} width={width} color={color} />
