import React from 'react'
import { styled, Box } from '@mui/material'
// Own components
import { MyTypography } from './MyTypography'
import { MyIcon } from './MyIcon'

const PREFIX = 'Info'

const classes = {
  root: `${PREFIX}-root`,
  icon: `${PREFIX}-icon`
}

const Root = styled(Box)({
  [`&.${classes.root}`]: {
    display: 'flex',
    alignItems: 'center',
    marginTop: 5
  },
  [`& .${classes.icon}`]: {
    marginRight: 10
  }
})

type InfoProps = {
  icon: object
  data: string
  color: string
}

export const Info: React.FC<InfoProps> = ({ icon, data, color }) => (
  <Root className={classes.root}>
    <Box className={classes.icon}>
      <MyIcon icon={icon} color={color} />
    </Box>
    <MyTypography variant="body2">{data}</MyTypography>
  </Root>
)
