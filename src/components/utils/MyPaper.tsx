import React, { ReactNode } from 'react'
import { styled, Box, Divider, Paper } from '@mui/material'

const PREFIX = 'MyPaper'

const classes = {
  paper: `${PREFIX}-paper`,
  rubrique: `${PREFIX}-rubrique`,
  divider: `${PREFIX}-divider`
}

const StyledPaper = styled(Paper, {
  shouldForwardProp: prop => prop !== 'paperStyle',
  name: 'MyThemeComponent',
  slot: 'StyledPaper'
})<StyledMyPaperStyleProps>(({ theme, paperStyle }) => ({
  [`&.${classes.paper}`]: {
    minWidth: 200,
    backgroundColor: paperStyle.backgroundColor,
    color: paperStyle.color,
    borderRadius: 15,
    border: paperStyle.border,
    margin: 10,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20,
    paddingTop: 10,
    boxShadow: paperStyle.boxShadow
  },
  [`& .${classes.rubrique}`]: {
    top: -20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  [`& .${classes.divider}`]: {
    marginTop: 10,
    marginBottom: 10
  }
}))

type StyledMyPaperStyleProps = {
  paperStyle: MyPaperStyleProps
}

type MyPaperStyleProps = {
  backgroundColor?: string
  boxShadow?: string | 'none'
  border?: string
  color?: string
}

type MyPaperProps = {
  rubrique: ReactNode
  icon?: ReactNode
  withDivider?: boolean
  paperStyle?: MyPaperStyleProps
  children: ReactNode
}

export const MyPaper: React.FC<MyPaperProps> = ({
  rubrique,
  icon,
  withDivider = true,
  paperStyle,
  children
}) => {
  // Style
  const defaultPaperStyle = {
    backgroundColor: 'black',
    boxShadow: 'none',
    border: 'none',
    color: 'white'
  }

  return (
    <StyledPaper
      className={classes.paper}
      paperStyle={{ ...defaultPaperStyle, ...paperStyle }}
    >
      <Box className={classes.rubrique}>
        {rubrique}
        {icon}
      </Box>
      {withDivider && (
        <Divider
          classes={{
            root: classes.divider
          }}
        />
      )}
      {children}
    </StyledPaper>
  )
}
