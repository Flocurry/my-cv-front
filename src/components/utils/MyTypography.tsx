import React, { ReactNode, CSSProperties } from 'react'
import { Typography, useTheme } from '@mui/material'

type MyTypographyProps = {
  variant?:
    | 'h1'
    | 'h2'
    | 'h3'
    | 'h4'
    | 'h5'
    | 'h6'
    | 'subtitle1'
    | 'subtitle2'
    | 'body1'
    | 'body2'
    | 'caption'
    | 'button'
    | 'overline'
    | 'inherit'
  customStyle?: CSSProperties
  children: ReactNode
}

export const MyTypography: React.FC<MyTypographyProps> = ({
  variant = 'body1',
  customStyle,
  children
}) => {
  // Theme
  const theme = useTheme()
  // Default style
  const defaultCustomStyle = {
    fontWeight: 400,
    color: theme.palette.text.primary
  }

  return (
    <Typography
      data-testid="my-typography"
      variant={variant}
      sx={{ ...defaultCustomStyle, ...customStyle }}
    >
      {children}
    </Typography>
  )
}
