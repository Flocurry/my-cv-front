import React, { useState } from 'react'
import {
  createTheme,
  ThemeProvider,
  StyledEngineProvider,
  PaletteMode
} from '@mui/material'
// Contexte
import { ModeContext } from './contextes/ModeContext'
// Custom palette
import { getPalette } from './themes/palettes'
// Own component
import { MyPortfolio } from './components/Presentational/MyPortfolio/MyPortfolio'

export const App: React.FC = () => {
  // State
  const [mode, setMode] = useState<PaletteMode>('dark')
  // Custom theme
  const theme = createTheme(getPalette(mode))

  const handleChangeMode = (param: PaletteMode) => {
    setMode(param)
  }

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <ModeContext.Provider
          value={{
            mode,
            setMode: param => handleChangeMode(param)
          }}
        >
          <MyPortfolio />
        </ModeContext.Provider>
      </ThemeProvider>
    </StyledEngineProvider>
  )
}
