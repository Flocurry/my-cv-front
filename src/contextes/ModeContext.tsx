import { createContext, useContext } from 'react'
import { PaletteMode } from '@mui/material'

export type ModeContextType = {
  mode: PaletteMode
  setMode: (mode: PaletteMode) => void
}

export const ModeContext = createContext<ModeContextType>({
  mode: 'dark',
  setMode: mode => {}
})

export const useModeContext = () => useContext(ModeContext)
